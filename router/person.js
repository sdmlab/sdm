const express= require('express')
const db=require('../db')
const utils=require('../util')
//const cryptoJs= require('crypto-js')
const router=express.Router()

router.get('/',(request,response)=>{
    const query=`select id,firstName,lastName,email,password from person`
    db.pool.execute(query,(error,persons)=>{
       response.send(utils.createResult(error,persons))
    })
})

router.post('/',(request,response)=>{
 const {firstName,lastName,email,password} = request.body

// const encryptedPassword = String(cryptoJs.MD5(password))
 const query= `insert into person (firstName,lastName,email,password) values (?,?,?,?)`
 db.pool.execute(query,[firstName,lastName,email,password],(error,result)=>{
    response.send(utils.createResult(error,result))
 })
})

router.delete('/',(request,response)=>{
    const {id}=request.body
    const query=`delete from person where id=?`
    db.pool.execute(query,[id],(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports=router